// 1
db.fruits.aggregate([
    {$match: { onSale: true}},
    {$group: {_id: "$name", total: {$count: {} }}}
]);

// 2
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$name", avgPrice: {$avg: "$price"}}}
]);

// 3
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", avgPrice: {$avg: "$price"}}}
]);