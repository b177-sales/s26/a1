// 1
db.fruits.aggregate([
    {$match: { onSale: true}},
    {$group: {_id: "$name", maxPrice: {$max: "$price"}}}
]);

// 2
db.fruits.aggregate([
    {$unwind: "$origin"},
    {$group: {_id: {origin: ["Philippines"]}, maxPrice: {$max: "$price"}}}
]);
