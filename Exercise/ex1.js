let http = require("http");

// Creates a variable "port" to store the port number
const port = 4001;

const server = http.createServer((request, response) => {
	if(request.url == '/welcome'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to the world of Node.js');
	}
	else if(request.url == '/register'){
		response.writeHead(503, {'Content-Type': 'text/plain'})
		response.end('Page is under maintenance');
	}
});

server.listen(port);

console.log(`Server is successfully running.`)